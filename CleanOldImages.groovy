import java.util.regex.*
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.sonatype.nexus.repository.storage.Asset
import org.sonatype.nexus.repository.storage.Component
import org.sonatype.nexus.repository.storage.StorageFacet
import org.sonatype.nexus.repository.storage.StorageTx
import groovy.json.JsonSlurper

class ReverseDateTimeComparator implements Comparator<DateTime> {
    @Override
    int compare(DateTime o1, DateTime o2) {
        return o2.compareTo(o1)
    }
}

int checked_assets = 0
int checked_components = 0
int checked_assets_with_null_blobupdate = 0
int checked_assets_with_no_component = 0
int checked_assets_removed = 0
int checked_components_removed = 0

//def request = new JsonSlurper().parseText(args)
def CurDate = new DateTime()
String repoName='docker.snapshot'
int numDays=26
// Boolean dryRun=false
Boolean dryRun=true
//assert request.repoName: 'repoName parameter is required'
//assert request.versionsOlderThan: 'versionsOlderThan parameter is required. Eg. versionsOlderThan=10.'
//boolean dryRun = request.dryRun.toBoolean() ?: false
//DateTime versionsOlderThan = CurDate.minusDays(request.versionsOlderThan.toInteger())
DateTime versionsOlderThan = CurDate.minusDays(numDays)

//log.info("Gathering Asset list for repository: {} with versionsOlderThan: {}, dryRun: {}",
//        request.repoName, versionsOlderThan, dryRun)
log.info("Gathering Asset list for repository: {} with versionsOlderThan: {}, dryRun: {}",
        repoName, versionsOlderThan, dryRun)


//def repo = repository.repositoryManager.get(request.repoName)
def repo = repository.repositoryManager.get(repoName)

//assert repo: "Repository ${request.repoName} does not exist"
assert repo: "Repository ${repoName} does not exist"
//assert repo.getFormat().getValue() == 'docker': "Repository ${request.repoName} is not docker"
assert repo.getFormat().getValue() == 'docker': "Repository ${repoName} is not docker"
log.info("Cleaning repository {}, format {}", repo.toString(), repo.getFormat().toString())

StorageTx storageTx = repo.facet(StorageFacet).txSupplier().get()
try {
    storageTx.begin()

    log.info("Collecting asset history")
    HashMap<String, SortedMap<DateTime, Component>> artifacts = new HashMap<String, SortedMap<DateTime, Asset>>()
    SortedMap<DateTime, Asset> sortedAssets
    ReverseDateTimeComparator reverseComparator = new ReverseDateTimeComparator()
    String gaString
    for (Asset asset : storageTx.browseAssets(storageTx.findBucket(repo))) {
      checked_assets++
      if (asset.componentId() != null) {
          checked_components++
          def component = storageTx.findComponent(asset.componentId());
          group = component.group()
          version = component.version()
        } else {
          checked_assets_with_no_component++
          group = 'nogroup'
          version ='no_version-latest'
        }
        gaString = sprintf("%s:%s", [group, asset.name()])
        log.info("BENGT group: {}", group)
        log.info("BENGT name: {}",  asset.name())
        log.info("BENGT version: {}", version)
        log.info("BENGT blob: {}", asset.blobUpdated())
        check_time = asset.blobUpdated()
        if (check_time == null) {
          checked_assets_with_null_blobupdate++
          blobcr = asset.blobCreated()
          lastdw = asset.lastDownloaded()
          check_time = lastdw
          log.info("BENGT blobcr: {}", blobcr)
          log.info("BENGT lastdw: {}", lastdw)
        }
        log.info("BENGT check_time: {}", check_time)

        if (artifacts.containsKey(gaString)) {
            sortedAssets = artifacts.get(gaString)
            sortedAssets.put(check_time, asset)

        } else {// first time
            sortedAssets = new TreeMap<DateTime, Asset>(reverseComparator)
            sortedAssets.put(check_time, asset)
            artifacts.put(gaString, sortedAssets)
        }
    }
    log.info("Found {} artifacts (groupId:artifactId)", artifacts.size())

    Asset asset
    for (String artifactString : artifacts.keySet()) {
        log.info("Processing artifact {} in repo {}", artifactString, repo.name)
        sortedAssets = artifacts.get(artifactString)
        Iterator assetIteratorReverse = sortedAssets.iterator().reverse()
        int versionsToProcess = sortedAssets.iterator().size()
        log.info("Number of assets to process: {}", versionsToProcess)

        // Assets are sorted by blob update date, last updated is printed last
        int kept=0
        while (assetIteratorReverse.hasNext()) {
            asset = assetIteratorReverse.next().getValue()
            if (asset.componentId() != null) {
              def component = storageTx.findComponent(asset.componentId());
              version = component.version()
              group = component.group()
            } else {
              version = 'no-version-latest'
              group = 'nogroup'
            }

            if (!(version==~ /.*-?latest$/) && (asset.blobUpdated() <= versionsOlderThan)) {
              if (!dryRun) {
                  log.info("SHOULD Deleting asset:     {}:{}:{}:{}", group, asset.name(), version, asset.blobUpdated())
                  checked_assets_removed++
                  // storageTx.deleteAsset(component)
                  if (asset.componentId() != null) {
                    checked_components_removed++
                    log.info("SHOULD Deleting component: {}:{}:{}:{}", group, asset.name(), version, asset.blobUpdated())
                    // storageTx.deleteComponent(component);
                  }
              } else {
                  checked_assets_removed++
                  log.info("Dry Run deleting asset:     {}:{}:{}:{}", group, asset.name(), version, asset.blobUpdated())
                  if (asset.componentId() != null) {
                    checked_components_removed++
                    log.info("Dry Run deleting component: {}:{}:{}:{}", group, asset.name(), version, asset.blobUpdated())
                  }
              }
            } else {
              log.info("Skipping newer or latest: {}:{}:{}:{}", group, asset.name(), version, asset.blobUpdated())
              kept++
            }
        }
        log.info("Assets kept: {}", kept)
        log.info("Assets removed: {}", versionsToProcess - kept)
    }
    log.info("=====================================================")
    log.info("=====================================================")
    log.info("Number of Assets checked:              {}", checked_assets)
    log.info("Number of Components checked:          {}", checked_components)
    log.info("Number of Assets with null blobUpdate: {}", checked_assets_with_null_blobupdate)
    log.info("Number of Assets with no Component:    {}", checked_assets_with_no_component)
    log.info("Assets removed:                        {}", checked_assets_removed)
    log.info("Components removed:                    {}", checked_components_removed)
    log.info("=====================================================")
    log.info("=====================================================")

    storageTx.commit()
}
catch (Exception e) {
    log.warn("Cleanup failed!!!")
    log.warn("Exception details: {}", e.toString())
    log.warn("Rolling back storage transaction")
    storageTx.rollback()
}
finally {
    storageTx.close()
}
