Modified groovy cleanup script for ONAP Nexus3.
The original is using lastUpdated, which apparently is not the best to use.
https://issues.sonatype.org/browse/NEXUS-12360

This version is now using blobUpdated instead.


Original version only checked components, and not the assets though.
Might be missmatch, with hanging assets?

    2021-07-06 12:57:56,590+0000 INFO  [quartz-13-thread-19]  *SYSTEM org.sonatype.nexus.internal.script.ScriptTask - Number of Assets checked:              126138
    2021-07-06 12:57:56,590+0000 INFO  [quartz-13-thread-19]  *SYSTEM org.sonatype.nexus.internal.script.ScriptTask - Number of Components checked:          15937
    2021-07-06 12:57:56,591+0000 INFO  [quartz-13-thread-19]  *SYSTEM org.sonatype.nexus.internal.script.ScriptTask - Number of Assets with null blobUpdate: 559
    2021-07-06 12:57:56,591+0000 INFO  [quartz-13-thread-19]  *SYSTEM org.sonatype.nexus.internal.script.ScriptTask - Number of Assets with no Component:    110201
    2021-07-06 12:57:56,591+0000 INFO  [quartz-13-thread-19]  *SYSTEM org.sonatype.nexus.internal.script.ScriptTask - Assets removed:                        2301
    2021-07-06 12:57:56,591+0000 INFO  [quartz-13-thread-19]  *SYSTEM org.sonatype.nexus.internal.script.ScriptTask - Components removed:                    2301
